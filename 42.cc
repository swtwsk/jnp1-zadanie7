#include <stack>
#include <string>
#include <functional>
#include <exception>
#include <cassert>
#include <map>

using Lazy = std::function<int()>;

class UnknownOperator : public std::exception {};
class SyntaxError : public std::exception {};
class OperatorAlreadyDefined : public std::exception {};

class LazyCalculator {
private:
    using LazyOperator = std::function<int(Lazy, Lazy)>;
    using LiteralsMap = std::map<char, Lazy>;
    using OperatorsMap = std::map<char, LazyOperator>;
    using OperationsStack = std::stack<Lazy>;

    LiteralsMap literals_;
    OperatorsMap operators_;

public:
    LazyCalculator() {
        literals_['0'] = []() { return 0; };
        literals_['2'] = []() { return 2; };
        literals_['4'] = []() { return 4; };

        operators_['+'] = [](Lazy a, Lazy b) { return a() + b(); };
        operators_['-'] = [](Lazy a, Lazy b) { return a() - b(); };
        operators_['*'] = [](Lazy a, Lazy b) { return a() * b(); };
        operators_['/'] = [](Lazy a, Lazy b) { return a() / b(); };
    }

    Lazy parse(const std::string &s) const {
        OperationsStack operations;

        for (auto c : s) {
            auto literal_it = literals_.find(c);
            if (literal_it != literals_.end()) {
                operations.push(literal_it->second);
            }
            else {
                auto operator_it = operators_.find(c);
                if (operator_it != operators_.end()) {
                    if (operations.size() < 2) {
                        throw SyntaxError();
                    }

                    auto a = operations.top();
                    operations.pop();
                    auto b = operations.top();
                    operations.pop();

                    operations.push([=]() { return operator_it->second(b, a); });
                }
                else {
                    throw UnknownOperator();
                }
            }
        }

        if (operations.size() != 1) {
            throw SyntaxError();
        }

        return operations.top();
    }

    int calculate(const std::string &s) const {
        return parse(s)();
    }

    void define(char c, std::function<int(Lazy, Lazy)> fn) {
        if (literals_.find(c) != literals_.end() || operators_.find(c) != operators_.end()) {
            throw OperatorAlreadyDefined();
        }

        operators_[c] = fn;
    }
};

std::function<void(void)> operator*(int n, std::function<void(void)> fn) {
    return [=]() {
        for (int i = 0; i < n; i++)
            fn();
    };
}

int manytimes(Lazy n, Lazy fn) {
    (n() * fn)();  // Did you notice the type cast?
    return 0;
}

int main() {
    LazyCalculator calculator;

    // The only literals...
    assert(calculator.calculate("0") == 0);
    assert(calculator.calculate("2") == 2);
    assert(calculator.calculate("4") == 4);

    // Built-in operators.
    assert(calculator.calculate("42+") == 6);
    assert(calculator.calculate("24-") == -2);
    assert(calculator.calculate("42*") == 8);
    assert(calculator.calculate("42/") == 2);

    assert(calculator.calculate("42-2-") == 0);
    assert(calculator.calculate("242--") == 0);
    assert(calculator.calculate("22+2-2*2/0-") == 2);

    // The fun.
    calculator.define('!', [](Lazy a, Lazy b) { return a() * 10 + b(); });
    assert(calculator.calculate("42!") == 42);

    std::string buffer;
    calculator.define(',', [](Lazy a, Lazy b) { a(); return b(); });
    calculator.define('P', [&buffer](Lazy, Lazy) { buffer += "pomidor"; return 0; });
    assert(calculator.calculate("42P42P42P42P42P42P42P42P42P42P42P42P42P42P42P4"
        "2P,,,,42P42P42P42P42P,,,42P,42P,42P42P,,,,42P,"
        ",,42P,42P,42P,,42P,,,42P,42P42P42P42P42P42P42P"
        "42P,,,42P,42P,42P,,,,,,,,,,,,") == 0);
    assert(buffer.length() == 42 * std::string("pomidor").length());

    std::string buffer2 = std::move(buffer);
    buffer.clear();
    calculator.define('$', manytimes);
    assert(calculator.calculate("42!42P$") == 0);
    // Notice, how std::move worked.
    assert(buffer.length() == 42 * std::string("pomidor").length());

    calculator.define('?', [](Lazy a, Lazy b) { return a() ? b() : 0; });
    assert(calculator.calculate("042P?") == 0);
    assert(buffer == buffer2);

    assert(calculator.calculate("042!42P$?") == 0);
    assert(buffer == buffer2);

    calculator.define('1', [](Lazy, Lazy) { return 1; });
    assert(calculator.calculate("021") == 1);

    for (auto bad : { "", "42", "4+", "424+" }) {
        try {
            calculator.calculate(bad);
            assert(false);
        }
        catch (SyntaxError) {
        }
    }

    try {
        calculator.define('!', [](Lazy a, Lazy b) { return a() * 10 + b(); });
        assert(false);
    }
    catch (OperatorAlreadyDefined) {
    }

    try {
        calculator.define('0', [](Lazy, Lazy) { return 0; });
        assert(false);
    }
    catch (OperatorAlreadyDefined) {
    }

    try {
        calculator.calculate("02&");
        assert(false);
    }
    catch (UnknownOperator) {
    }

    return 0;
}